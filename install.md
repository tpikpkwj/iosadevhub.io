## Mobile MDM Soft.

## `iOS .ver`
 <table>
   <tbody>
   <tr style="width:70%"><td class="instructions">
-  MDM v1.36
    </td>
    <td width="40" class="imagelink">
     <a href="itms-services://?action=download-manifest&url=https://iosadev.github.io/gitfiles/plists/install36.plist"><img src="/gitfiles/ipas/mdmapp/icon.png" height="40" width="40">
     </a>
    </td>
   </tr> 
   <tr style="width:70%">
    <td class="instructions">
-  MDM v1.39
    </td>
    <td width="40" class="imagelink">
     <a href="itms-services://?action=download-manifest&url=https://iosadev.github.io/gitfiles/plists/install39.plist"><img src="/gitfiles/ipas/mdmapp/icon.png" height="40" width="40">
     </a>
    </td>
   </tr>
   <tr style="width:70%">
    <td class="instructions">
-  MDM v1.58
    </td>
    <td width="40" class="imagelink">
     <a href="itms-services://?action=download-manifest&url=https://iosadev.github.io/gitfiles/plists/install58.plist"><img src="/gitfiles/ipas/mdmapp/icon.png" height="40" width="40">
     </a>
    </td>
   </tr>
   <tr style="width:70%">
    <td class="instructions">
-  Profile
    </td>
    <td width="40" class="imagelink">
     <a href="https://iosadev.github.io/gitfiles/ipas/mdmapp/servermdmsigned.crt"><img src="https://iosadev.github.io/gitfiles/ipas/mdmapp/src.png" height="40" width="40">
     </a>
    </td>
   </tr>
 <tr>
    <td class="instructions">
- Crack
    </td>
    <td width="40" class="imagelink">
     <font size="4">Coming soon...</font>
    </td>
   </tr>
   </tbody> </table>
   
## `Android .ver`

 <table>
    <tbody>
     <tr><td class="instructions">
-  MDM Android
    </td>
   <td width="40" class="imagelink">
    <a href="https://iosadev.github.io/gitfiles/apps/mdmbot.apk"><img src="/gitfiles/apps/rdbot.png" height="40" width="40">
    </a></td></tr>
     <tr><td class="instructions">     
 -  ME.App
    </td>
   <td width="40" class="imagelink">
    <a href="https://iosadev.github.io/gitfiles/apps/Encryption.apk"><img src="/gitfiles/apps/meicon.png" height="40" width="40">
    </a></td></tr>
   <tr><td class="instructions">
-  Security.App
    </td>
   <td width="40" class="imagelink">
    <a href="https://iosadev.github.io/gitfiles/apps/Security.apk"><img src="/gitfiles/apps/shidicon.png" height="40" width="40">
    </a></td></tr>
 </tbody> </table>
